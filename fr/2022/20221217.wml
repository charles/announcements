#use wml::debian::translation-check translation="XXXXX maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.6</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la sixième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction awstats "Correction d'un problème de script intersite [CVE-2022-46391]">
<correction base-files "Mise à jour de /etc/debian_version pour la version 11.6">
<correction binfmt-support "Exécution de binfmt-support.service après systemd-binfmt.service">
<correction clickhouse "Correction de problèmes de lecture hors limites [CVE-2021-42387 CVE-2021-42388] et de problèmes de dépassement de tampon [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "Greffon CRI : Correction de fuite de goroutine durant Exec [CVE-2022-23471]">
<correction core-async-clojure "Correction d'échecs de construction dans la suite de tests">
<correction dcfldd "Correction de sortie SHA1 sur les architectures gros-boutistes">
<correction debian-installer "Reconstruction avec proposed-updates ; passage de l'ABI du noyau Linux à la version 5.10.0-20">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debmirror "Ajout de non-free-firmware à la liste de sections par défaut">
<correction distro-info-data "Ajout d'Ubuntu 23.04, Lunar Lobster ; mise à jour des dates de fin de Debian ELTS ; correction de la date de publication de Debian 8 (Jessie)">
<correction dojo "Correction d'un problème de corruption de prototype [CVE-2021-23450]">
<correction dovecot-fts-xapian "Génération d'une dépendance à la version de l'ABI dovecot utilisée durant la construction">
<correction efitools "Correction d'un échec de construction intermittent dû à une dépendance incorrecte dans makefile">
<correction evolution "Migration des carnets d'adresses Contacts de Google vers CalDAV dans la mesure où l'API de Contacts de Google a été abandonnée">
<correction evolution-data-server "Migration des carnets d'adresses Contacts de Google vers CalDAV dans la mesure où l'API de Contacts de Google a été abandonnée ; correction de compatibilité avec les modifications de OAuth de Gmail">
<correction evolution-ews "Correction de la récupération des certificats d'utilisateur appartenant à Contacts">
<correction g810-led "Contrôle d'accès au périphérique avec uaccess plutôt que de tout rendre accessible à tous en écriture [CVE-2022-46338]">
<correction glibc "Correction d'une régression dans wmemchr et wcslen sur les processeurs disposant d'AVX2 mais pas de BMI2 (par exemple Haswell d'Intel)">
<correction golang-github-go-chef-chef "Correction d'un échec de test intermittent">
<correction grub-efi-amd64-signed "Pas de retrait des symboles des binaires de Xen, afin qu'ils fonctionnent à nouveau ; inclusion des fontes dans la construction de memdisk pour les images EFI ; correction d'un bogue dans le code du fichier core de sorte que les erreurs sont mieux gérées ; passage au niveau 4 du SBAT de Debian">
<correction grub-efi-arm64-signed "Pas de retrait des symboles des binaires de Xen, afin qu'ils fonctionnent à nouveau ; inclusion des fontes dans la construction de memdisk pour les images EFI ; correction d'un bogue dans le code du fichier core de sorte que les erreurs sont mieux gérées ; passage au niveau 4 du SBAT de Debian">
<correction grub-efi-ia32-signed "Pas de retrait des symboles des binaires de Xen, afin qu'ils fonctionnent à nouveau ; inclusion des fontes dans la construction de memdisk pour les images EFI ; correction d'un bogue dans le code du fichier core de sorte que les erreurs sont mieux gérées ; passage au niveau 4 du SBAT de Debian">
<correction grub2 "Pas de retrait des symboles des binaires de Xen, afin qu'ils fonctionnent à nouveau ; inclusion des fontes dans la construction de memdisk pour les images EFI ; correction d'un bogue dans le code du fichier core de sorte que les erreurs sont mieux gérées ; passage au niveau 4 du SBAT de Debian">
<correction hydrapaper "Ajout de dépendance manquante à python3-pil">
<correction isoquery "Correction d'un échec de test provoqué par une modification de la traduction française dans le paquet iso-codes">
<correction jtreg6 "Nouveau paquet requis pour la construction des nouvelles versions d'openjdk-11">
<correction lemonldap-ng "Amélioration de la propagation de la destruction de session [CVE-2022-37186]">
<correction leptonlib "Correction d'une division par zéro [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Correction d'un problème de redirection ouverte [CVE-2021-3639]">
<correction libbluray "Correction de la prise en charge de BD-J avec les mises à jour récentes de Java d'Oracle">
<correction libconfuse "Correction de lecture hors limites de tampon de tas dans cfg_tilde_expand [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libtasn1-6 "Correction d'un problème d'écriture hors limites [CVE-2021-46848]">
<correction libvncserver "Correction d'une fuite de mémoire [CVE-2020-29260] ; prise en charge de plus grandes tailles d'écran">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 20 ; [rt] mise à jour vers la version 5.10.158-rt77">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 20 ; [rt] mise à jour vers la version 5.10.158-rt77">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 20 ; [rt] mise à jour vers la version 5.10.158-rt77">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 20 ; [rt] mise à jour vers la version 5.10.158-rt77">
<correction mariadb-10.5 "Nouvelle version amont stable ; corrections de sécurité [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "Suppression de l'en-tête X-Client-IP quand ce n'est pas un en-tête fiable [CVE-2022-2255]">
<correction mplayer "Correction de plusieurs problèmes de sécurité [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "Correction d'un plantage de gpgme lors de l'énumération des clés dans un <q>public key block</q>, et liste de <q>public key block<q> pour les versions anciennes de gpgme">
<correction nano "Correction de plantages et d'un problème potentiel de perte de données">
<correction nftables "Correction d'une erreur de décalage d'entier et de double libération">
<correction node-hawk "Analyse des URL en utilisant stdlib [CVE-2022-29167]">
<correction node-loader-utils "Correction d'un problème de corruption de prototype [CVE-2022-37599 CVE-2022-37601] et d'un problème de déni de service basé sur les expressions rationnelles [CVE-2022-37603]">
<correction node-minimatch "Amélioration de la protection contre un déni de service basé sur les expressions rationnelles [CVE-2022-3517] ; correction d'une régression dans le correctif pour le CVE-2022-3517">
<correction node-qs "Correction d'un problème de corruption de prototype [CVE-2022-24999]">
<correction node-xmldom "Correction d'un problème de corruption de prototype [CVE-2022-37616] ; insertion de nœuds mal formés empêchée [CVE-2022-39353]">
<correction nvidia-graphics-drivers "Nouvelle version amont ; corrections de sécurité [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont ; corrections de sécurité [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont ; corrections de sécurité [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont ; corrections de sécurité [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Ajout d'une dépendance manquante à libjs-jquery pour le paquet omnievents-doc">
<correction onionshare "Correction d'un problème de déni de service [CVE-2022-21689] et d'un problème d'injection HTML [CVE-2022-21690]">
<correction openvpn-auth-radius "Prise en charge de la directive verify-client-cert">
<correction postcorrection de "Nouvelle version amont stable">
<correction postgresql-13 "Nouvelle version amont stable">
<correction powerline-gitstatus "Correction d'une injection de commande au moyen d'une configuration de dépôt malveillante [CVE-2022-42906]">
<correction pysubnettree "Correction de construction du module">
<correction speech-dispatcher "Réduction de la taille du tampon d'espeak pour éviter des artefacts de synthèse vocale">
<correction spf-engine "Correction d'échec de démarrage de pyspf-milter dû à une déclaration d'importation non valable">
<correction tinyexr "Correction de problèmes de dépassement de tas [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Correction d'une boucle infinie [CVE-2021-42260]">
<correction tzdata "Mise à jour des données pour les Fidji, le Mexique et la Palestine ; mise à jour de la liste des secondes intercalaires">
<correction virglrenderer "Correction d'un problème d'écriture hors limites [CVE-2022-0135]">
<correction x2gothinclient "Fourniture du paquet virtuel lightdm-greeter par le paquet x2gothinclient-minidesktop">
<correction xfig "Correction d'un problème de dépassement de tampon [CVE-2021-40241]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
