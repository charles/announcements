<define-tag pagetitle>Debian Edu/Skolelinux Buster — une solution Linux complète pour votre école</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news

#use wml::debian::translation-check translation="1.1" maintainer="Jean-Pierre Giraud"

<p>
Vous avez à administrer un atelier informatique ou un réseau pour toute une
école ? Vous voulez installer des serveurs, des stations de travail et
des ordinateurs portables qui fonctionnent ensemble ?
Voulez-vous bénéficier de la stabilité de Debian avec des services réseau
déjà configurés ?
Souhaitez-vous avoir un outil web pour gérer le système et plusieurs
centaines, voire plus, d'utilisateurs ?
Vous êtes-vous déjà demandé si et comment de vieux ordinateurs peuvent
être utilisés ?
</p>

<p>
Dans ce cas, Debian Edu est faite pour vous. Les enseignants eux-mêmes ou
leur assistance technique peuvent déployer un environnement scolaire complet
multi-utilisateur et multi-machine en quelques jours. Debian Edu propose des
centaines d'applications pré-installées, mais il est toujours possible
d'ajouter plus de paquets à partir de Debian.
</p>

<p>
L'équipe de développement de Debian Edu est heureuse d'annoncer Debian
Edu 10 <q>Buster</q>, la première version de Debian Edu/Skolelinux,
basée sur Debian 10 <q>Buster</q>. Vous pourriez envisager de la tester et
rapporter les problèmes (&lt;debian-edu@lists.debian.org&gt;) pour nous aider
à encore l'améliorer.
</p>

<h2>À propos de Debian Edu et de Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu">Debian Edu, également connue sous le
nom de Skolelinux</a>, est une distribution Linux basée sur Debian
fournissant un environnement entièrement préconfiguré pour un réseau
scolaire. Immédiatement après l'installation, un serveur faisant tourner
tous les services requis par un réseau scolaire est installé et n'attend que
l'ajout d'utilisateurs et de machines avec GOsa², une interface web simple
d'utilisation. Un environnement de démarrage par le réseau est préparé,
aussi, après l'installation initiale du serveur principal à partir d'un CD,
DVD, BD ou d'une clé USB, toutes les autres machines peuvent être installées
à travers le réseau.
Les vieux ordinateurs (même ceux d'une dizaine d'années ou plus) peuvent
être utilisés comme clients légers LTSP ou stations de travail sans disque,
démarrant à partir du réseau sans besoin d'aucune installation ou configuration.
Le réseau d'école Debian Edu fournit une base de données LDAP et un service
d'authentification Kerberos, des répertoires personnels centralisés, un
serveur DHCP, un serveur mandataire web et de nombreux autres services.
Le bureau contient plus de 60 paquets de logiciels éducatifs et beaucoup
d'autres sont disponibles dans l'archive Debian.
Les écoles peuvent choisir entre les environnements de bureau Xfce, GNOME, LXDE,
MATE, KDE Plasma et LXQt.
</p>

<h2>Nouvelles fonctionnalités de Debian Edu 10 <q>Buster</q></h2>

<p>Voici quelques points issus des notes de publication de Debian Edu 10,
<q>Buster</q>, basée sur la version Debian 10 <q>Buster</q>. La liste complète
comprenant des informations plus détaillées est incluse dans le chapitre
correspondant du
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">manuel de Debian Edu</a>.
</p>

<ul>
<li>
Images officielles d'installation de Debian maintenant disponibles.
</li>
<li>
Installation modulaire spécifique au site possible.
</li>
<li>
Fourniture de métapaquets supplémentaires regroupant les paquets éducatifs par
niveau scolaire.
</li>
<li>
Amélioration de la localisation des bureaux pour toutes les langues prises en
charge par Debian.
</li>
<li>
Outil disponible pour faciliter la prise en charge multilingue spécifique au
site.
</li>
<li>
Ajout du greffon gestionnaire de mots de passe GOsa².
</li>
<li>
Prise en charge améliorée de TLS/SSL dans le réseau interne.
</li>
<li>
Prise en charge des services NFS et SSH par la configuration de Kerberos.
</li>
<li>
Outil pour recréer la base de données LDAP disponible.
</li>
<li>
Installation d'un serveur X2Go sur tous les systèmes ayant le profil
« LTSP-Server ».
</li>
</ul>

<h2>Options de téléchargement, étapes d'installation et manuel</h2>

<p>
Des images CD distinctes d'installation par le réseau pour les PC 64 bits et
32 bits sont disponibles. Les images 32 bits ne seront nécessaires qu'en de
rares cas (pour les PC plus anciens qu'une douzaine d'années). Les images
peuvent être téléchargées aux emplacements suivants :
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Sinon, des images BD plus grandes (plus de 5 Go) sont aussi disponibles. Il est
possible de mettre en place un réseau Debian Edu complet sans connexion Internet
(pour tous environnements de bureau et toutes langues pris en charge par Debian).
Ces images peuvent être téléchargées aux emplacements suivants :
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Les images peuvent être vérifiées avec les sommes de contrôle fournies dans le
répertoire de téléchargement.
<br />
Une fois que vous avez téléchargé une image, vous pouvez vérifier que :
<ul>
<li>
sa somme de contrôle correspond à celle attendue à partir du fichier de somme de contrôle ;
</li>
<li>
le fichier de somme de contrôle n'a pas été falsifié.
</li>
</ul>
Pour plus d'informations sur comment suivre ces étapes, consultez la page
<a href="https://www.debian.org/CD/verify">Vérification de l'authenticité des CD Debian</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> repose totalement sur Debian 10 <q>Buster</q> ;
aussi les sources de tous les paquets sont disponibles dans l'archive Debian.
</p>

<p>
Veuillez noter la
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">page d'état de Debian Edu Buster
pour des informations actualisées sur Debian Edu 10 <q>Buster</q> y compris des
instructions sur comment utiliser <code>rsync</code> pour télécharger les images ISO.</a>
</p>

<p>
Lors d'une mise à niveau à partir de Debian Edu 9 <q>Stretch</q>, veuillez
consulter le
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">chapitre du manuel de Debian Edu</a> correspondant.
</p>

<p>
Pour des notes d'installation, lisez le
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">chapitre du manuel de Debian Edu</a>
correspondant.
</p>

<p>
Après l'installation, il faut suivre ces
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">premières étapes.</a>
</p>

<p>
Veuillez consulter les
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">pages wiki de Debian Edu</a>
pour trouver la dernière version anglaise du manuel de Debian Edu
<q>Buster</q>.
Le manuel a été entièrement traduit en allemand, français, italien, danois,
néerlandais, norvégien bokmål et japonais. Des versions partiellement traduites
en espagnol et en chinois simplifié existent également.
Un récapitulatif <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
des dernières versions traduites du manuel</a> est disponible.
</p>

<p>
Plus d'informations sur Debian 10 <q>Buster</q> elle-même sont fournies dans
les notes de publication et le manuel d'installation ; voir
<a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>À propos de Debian</h2>

<p>Le projet Debian est une organisation de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Coordonnées</h2>

<p>Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt;.</p>
