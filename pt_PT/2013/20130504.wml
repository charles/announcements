# Status: [content-frozen]

<define-tag pagetitle>Foi lançado Debian 7.0 <q>Wheezy</q></define-tag>
<define-tag release_date>2013-05-04</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
#use wml::debian::translation-check translation="1.1" maintainer="Miguel Figueiredo"


<p>
Após muitos meses de constante desenvolvimento, o projecto Debian está 
orgulhoso por apresentar a nova versão estável 7.0 (nome de código 
<q>Wheezy</q>).
<br />
Esta nova versão de Debian inclui várias funcionalidades interessantes tais 
como <a href="http://www.debian.org/News/2011/20110726b">suporte multiarch
</a>, várias <a href="http://www.debian.org/News/2012/20120425">ferramentas 
específicas para instalar clouds privadas</a>, um instalador melhorado, e um 
conjunto completo de codecs multimedia e front-ends que acabam com a 
necessidade de repositórios de terceiros.
</p>


<p>
O suporte Multiarch, um dos principais objectivos de lançamento para o 
<q>Wheezy</q>, irá permitir aos utilizadores de Debian instalarem pacotes de 
várias arquitecturas na mesma máquina. Isto significa que a partir de agora 
pode, pela primeira vez, instalar software de 32- e 64-bit na mesma máquina e 
ter as dependências relevantes correctamente resolvidas, automaticamente.
</p>

 
<p>
O processo de instalação foi bastante melhorado: Debian pode agora ser 
instalado utilizando software de síntese de voz, dirigido principalmente aos 
deficientes visuais que não utilizem um dispositivo Braille. Graças ao esforço 
combinado de um largo número de tradutores, o sistema de instalação está 
disponível agora em 73 idiomas, e mais de uma dúzia deles estão também 
disponíveis para síntese de voz.
<br />
Além diso, pela primeira vez, Debian suporta a instalação e o arranque 
utilizando UEFI para os novos computadores de 64-bit (<code>amd64</code>), 
apesar de ainda não existir suporte para <q>Secure Boot</q>.
</p>

<p>
Este lançamento inclui numerosos pacotes de software actualizado, tais como:
</p>
<ul>
<li>Apache 2.2.22 </li>
<li>Asterisk 1.8.13.1 </li>
<li>GIMP 2.8.2 </li>
<li>uma versão actualizada do ambiente de trabalho GNOME 3.4 </li>
<li>GNU Compiler Collection 4.7.2 </li>
<li>Icedove 10 (uma versão sem marca do Mozilla Thunderbird)</li>
<li>Iceweasel 10 (uma versão sem marca do Mozilla Firefox)</li>
<li>KDE Plasma Workspaces e KDE Applications 4.8.4 </li>
<li>LibreOffice 3.5.4 </li>
<li>Linux 3.2 </li>
<li>MySQL 5.5.30 </li>
<li>Nagios 3.4.1 </li>
<li>OpenJDK 6b27 e 7u3 </li>
<li>Perl 5.14.2 </li>
<li>PHP 5.4.4 </li>
<li>PostgreSQL 9.1 </li>
<li>Python 2.7.3 e 3.2.3 </li>
<li>Samba 3.6.6 </li>
<li>Tomcat 6.0.35 e 7.0.28 </li>
<li>Xen Hypervisor 4.1.4 </li>
<li>o ambiente de trabalho Xfce 4.8</li>
<li>X.Org 7.7 </li>
<li>mais de outros 36,000 pacotes de software prontos a utilizar, compilados a partir de quase 17,500 pacotes de código fonte.</li>
</ul>

<p>
Com esta larga selecção de pacotes, Debian uma vez mais mantém-se fiel aos 
seus objectivos de ser um sistema operativo universal. É adequado para mais 
casos de diferentes utilizações: desde ambientes de trabalho para netbooks; 
de servidores de desenvolvimento a sistemas cluster; e para bases de dados, 
web ou servidores de armazenamento. Ao mesmo tempo, esforços adicionais de 
garantia de qualidade como instalação automática e testes de actualizações 
para todos os pacotes no arquivo do Debian asseguram que o <q>Wheezy</q> 
preenche as altas expectativas que os utilizadores têm de um lançamento 
estável de Debian. É seguro como pedra e rigorosamente testado.
</p>

<p>
Pode instalar Debian em computadores desde systemas móveis a 
super-computadores, e praticamente tudo no meio.
É suportado um total de 9 arquitecturas:
32-bit PC / Intel IA-32 (<code>i386</code>), 64-bit PC / Intel EM64T
/ x86-64 (<code>amd64</code>), Motorola/IBM PowerPC (<code>powerpc</code>),
Sun/Oracle SPARC (<code>sparc</code>), MIPS (<code>mips</code> (big-endian) e
<code>mipsel</code> (little-endian)), Intel Itanium (<code>ia64</code>), IBM
S/390 (31-bit <code>s390</code> e 64-bit <code>s390x</code>), e ARM EABI
(<code>armel</code> para hardware mais antigo e <code>armhf</code> para hardware mais recente com vírgula-flutuante).
</p>

<p>
Quer experimentar?
<br />
Se simplesmente quer tentar sem ser obrigado a instalar, pode utilizar uma 
imagem especial, conhecida como live image, disponível para CDs, pens USB, e 
configurações netboot. Inicialmente, essas imagens são disponibilizadas apenas 
para as arquitecturas <code>amd64</code> e <code>i386</code>. Também é 
possível utilizar essas imagens para instalar Debian. Está disponível mais 
informação a partir da 
<a href="http://live.debian.net/">Debian Live homepage</a>.
</p>

<p>
Se, em vez disso, quiser instalar directamente, pode escolher entre vários 
meios de instalação, tais como Blu-Ray Discs, DVDs, CDs, e pens USB ou a 
partir da rede. Alguns ambientes de trabalho &mdash; GNOME, KDE Plasma Desktop 
and Applications, Xfce, e LXDE &mdash; podem ser instalado através das imagens 
de CD; o desejado é escolhido nos menus de arranque dos CDs/DVDs. Além disso, 
estão disponíveis os CDS e DVDs multi-arquitectura, os quais suportam a 
instalação de várias arquitecturas a partir de um único disco. Ou pode sempre 
criar pens de arranque para instalação (para mais detalhes veja o 
<a href="$(HOME)/releases/wheezy/installmanual">Guia de Instalação</a>. 
</p>

<p>
Pode ser feito o download das imagens de instalação agora através de 
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (o método recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; para mais informações veja
<a href="$(HOME)/CD/">Debian em CDs</a>.  O Wheezy também irá brevemente estar 
disponível em DVD, CD-ROM, and Blu-ray Discs físicos a partir de vários 
<a href="$(HOME)/CD/vendors">distribuidores</a>.
</p>

<p>
Já é um feliz utilizador de Debian e apenas deseja fazer a actualização?
<br />
As actualizações para Debian 7.0 a partir do lançamento anterior, Debian 6.0 
(nome de código <q>Squeeze</q>), são automaticamente tratadas pela ferramenta 
de gestão de pacotes apt-get para a maioria das configurações. Como sempre, os 
sistemas Debian podem ser actualizados sem esforço, no lugar, sem 
indisponibilidade forçada, mas é fortemente recomendado que leia as 
<a href="$(HOME)/releases/wheezy/releasenotes">notas de lançamento</a> assim 
como o <a href="$(HOME)/releases/wheezy/installmanual">guia de instalação</a> 
para possíveis problemas, assim como para instruções detalhadas de instalação 
e de actualização. As notas de lançamento serão ainda mais melhoradas e 
traduzidas em mais idiomas nas semanas após o lançamento.
</p>


<h2>Sobre Debian</h2>

<p>
Debian é um sistema operativo livre, desenvolvido por milhares de voluntários 
de todo o mundo que colaboram através da Internet. Um dos pontos chave do 
projecto Debian é a sua base de voluntários, a sua dedicação ao Contrato 
Social Debian e ao Software Livre, assim como o seu compromisso para fornecer 
o melhor sistema operativo possível. Debian 7.0 é outro importante passo nessa 
direcção.
</p>


<h2>Informação de Contacto</h2>

<p>
Para mais informações, por favor visite as páginas web Debian em 
<a href="$(HOME)/">http://www.debian.org/</a> ou envie um email para 
&lt;press@debian.org&gt;.
</p>
