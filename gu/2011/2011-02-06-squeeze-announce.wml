# The content of this announcement has been frozen
# please refrain from changing it, but feel free to fix gramar / typoes /
# style (if not intrusive)

<define-tag pagetitle>ડેબિયન ૬.૦ <q>Squeeze</q> પ્રસ્તુત</define-tag>
<define-tag release_date>૨૦૧૧-૦૨-૦૬</define-tag>
#use wml::debian::news

<p>૨૪ મહિનાઓના સળંગ ડેવલોપમેન્ટ પછી, ડેબિયન પ્રોજેક્ટ ગર્વપૂર્વક તેના નવી સ્થિત આવૃત્તિ ૬.૦
(કોડ નામ <q>Squeeze</q>) રજૂ કરે છે. ડેબિયન ૬.૦ એ મુક્ત ઓપરેટિંગ સિસ્ટમ છે, જે સૌ પ્રથમ
વખત બે સ્વરૂપોમાં રજૂ થાય છે. ડેબિયન ગ્નુ/લિનક્સની સાથે ગ્નુ/kFreeBSD આ આવૃત્તિની સાથે
<q>ટેકનોલોજી પૂર્વદર્શન</q> તરીકે પ્રસ્તુત કરવામા આવેલ છે.</p>

<p>ડેબિયન ૬.૦ કેડીઈ પ્લાઝમા ડેસ્કટોપ અને કાર્યક્રમો, ગ્નોમ, Xfce, અને LXDE ડેસ્કટોપ તેમજ બધાં
પ્રકારનાં સર્વર કાર્યક્રમો ધરાવે છે.  તે FHS v2.3 સાથે સુસંગતતા અને LSB ની ૩.૨ આવૃત્તિ માટે બનાવેલ
સોફ્ટવેર ધરાવે છે.</p>

<p>Debian runs on computers ranging from palmtops and handheld systems
to supercomputers, and on nearly everything in between.  A total of nine
architectures are supported by Debian GNU/Linux: 32-bit PC / Intel IA-32
(<code>i386</code>), 64-bit PC / Intel EM64T / x86-64
(<code>amd64</code>), Motorola/IBM PowerPC (<code>powerpc</code>),
Sun/Oracle SPARC (<code>sparc</code>), MIPS (<code>mips</code>
(big-endian) and <code>mipsel</code> (little-endian)), Intel Itanium
(<code>ia64</code>), IBM S/390 (<code>s390</code>), and ARM EABI
(<code>armel</code>).</p>

<p>Debian 6.0 <q>Squeeze</q> introduces technical previews of two new
ports to the kernel of the FreeBSD project using the known Debian/GNU userland:
Debian GNU/kFreeBSD for the 32-bit PC (<code>kfreebsd-i386</code>) and
the 64-bit PC (<code>kfreebsd-amd64</code>). These ports are the first
ones ever to be included in a Debian release which are not based on
the Linux kernel. The support of common server software is strong and
combines the existing features of Linux-based Debian versions with the
unique features known from the BSD world. However, for this release
these new ports are limited; for example, some advanced desktop
features are not yet supported.</p>

<p>Another first is the completely free Linux kernel, which no longer
contains problematic firmware files. These were split out into
separate packages and moved out of the Debian main archive into the
non-free area of our archive, which is not enabled by default. In this
way Debian users have the possibility of running a completely free
operating system, but may still choose to use non-free firmware files
if necessary. Firmware files needed during installation may be loaded
by the installation system; special CD images and tarballs for USB
based installations are available too.  More information about this
may be found in the Debian <a
href="http://wiki.debian.org/Firmware">Firmware wiki page</a>.</p>

<p>Furthermore, Debian 6.0 introduces a dependency based boot system,
making system start-up faster and more robust due to parallel
execution of boot scripts and correct dependency tracking between
them. Various other changes make Debian more suitable for small form
factor notebooks, like the introduction of the KDE Plasma Netbook
shell.</p>

<p>આ આવૃત્તિ સંખ્યાબંધ સોફ્ટવેર પકેજીસ ધરાવે છે, જેવાં કે
<ul>
<li>કેડીઈ પ્લાઝમા વર્કસ્પેસ અને કેડીઈ કાર્યક્રમો ૪.૪.પ</li>
<li>ગ્નોમ ડેસ્કટોપ ૨.૩૦ની સુધારેલી આવૃત્તિ</li>
<li>Xfce ૪.૬ ડેસ્કટોપ</li>
<li>LXDE ૦.૫.૦</li>
<li>X.Org ૭.૫</li>
<li>ઓપનઓફિસ.ઓર્ગ ૩.૨.૧</li>
<li>જીમ્પ ૨.૬.૧૧</li>
<li>Iceweasel ૩.પ.૧૬ (મોઝ્ઝિલા ફાયરફોક્સની છાપ વગરની આવૃત્તિ)</li>
<li>Icedove ૩.૦.૧૧ (મોઝ્ઝિલા થન્ડરબર્ડની છાપ વગરની આવૃત્તિ)</li>
<li>PostgreSQL ૮.૪.૬</li>
<li>MySQL ૫.૧.૪૯</li>
<li>ગ્નુ કમ્પાઈલર સમૂહ ૪.૪.પ</li>
<li>લિનક્સ ૨.૬.૩૨</li>
<li>અપાચે ૨.૨.૧૬</li>
<li>સામ્બા ૩.૫.૬</li>
<li>પાયથોન ૨.૬.૬, ૨.૫.૫ અને ૩.૧.૩</li>
<li>પર્લ ૫.૧૦.૧</li>
<li>PHP ૫.૩.૩</li>
<li>એસ્ટ્રિક્સ ૧.૬.૨.૯</li>
<li>Nagios ૩.૨.૩</li>
<li>Xen Hypervisor ૪.૦.૧ (dom0 તેમજ domU આધાર પણ)</li>
<li>OpenJDK ૬b૧૮</li>
<li>ટોમકેટ ૬.૦.૧૮</li>
<li>લગભગ ૧૫,૦૦૦ સ્ત્રોત પેકેજોમાંથી બનાવેલ, ૨૯,૦૦૦ કરતાં વધુ વાપરવા માટે તૈયાર પેકેજો.</li>
</ul>
Debian 6.0 includes over 10,000 new packages like the browser Chromium,
the monitoring solution Icinga, the package management frontend Software
Center, the network manager wicd, the Linux container tools lxc and the
cluster framework corosync.
</p>

<p>With this broad selection of packages, Debian once again stays true
to its goal of being the universal operating system. It is suitable
for many different use cases: from desktop systems to netbooks; from
development servers to cluster systems; and for database, web or
storage servers. At the same time, additional quality assurance
efforts like automatic installation and upgrade tests for all packages
in Debian's archive ensure that Debian 6.0 fulfils the high
expectations that users have of a stable Debian release. It is rock
solid and rigorously tested.
</p>

<p>Starting from Debian 6.0, the <q>Custom Debian Distributions</q> are
renamed to <a href="http://blends.alioth.debian.org/"><q>Debian Pure
Blends</q></a>. Their coverage has increased as Debian 6.0 adds <a
href="http://www.debian.org/devel/debian-accessibility/">Debian
Accessibility</a>, <a
href="http://debichem.alioth.debian.org/">DebiChem</a>,
<a href="http://wiki.debian.org/DebianEzGo">Debian EzGo</a>, <a
href="http://wiki.debian.org/DebianGis">Debian GIS</a> and <a
href="http://blends.alioth.debian.org/multimedia/tasks/index">Debian
Multimedia</a> to the already existing <a
href="http://wiki.debian.org/DebianEdu">Debian Edu</a>, <a
href="http://www.debian.org/devel/debian-med/">Debian Med</a> and <a
href="http://wiki.debian.org/DebianScience">Debian Science</a> <q>pure
blends</q>. The full content of all the blends <a
href="http://blends.alioth.debian.org/">can be browsed</a>, including
prospective packages that users are welcome to nominate for addition to the
next release.</p>

<p>Debian may be installed from various installation media such as
Blu-ray Discs, DVDs, CDs and USB sticks or from the network.  GNOME is
the default desktop environment and is contained on the first
CD. Other desktop environments &mdash; KDE Plasma Desktop and
Applications, Xfce, or LXDE &mdash; may be installed through two
alternative CD images. The desired desktop environment may also be
chosen from the boot menus of the CDs/DVDs.  Again available with
Debian 6.0 are multi-architecture CDs and DVDs which support
installation of multiple architectures from a single disc.  The
creation of bootable USB installation media has also been greatly
simplified; see the <a href="$(HOME)/releases/squeeze/installmanual">Installation Guide</a> for more details.</p>

<p>In addition to the regular installation media, Debian GNU/Linux may
also be directly used without prior installation.  The special images
used, known as live images, are available for CDs, USB sticks and
netboot setups. Initially, these are provided for the
<code>amd64</code> and <code>i386</code> architectures only. It is
also possible to use these live images to install Debian
GNU/Linux.</p>

<p>The installation process for Debian GNU/Linux 6.0 has been improved
in various ways, including easier selection of language and keyboard
settings, and partitioning of logical volumes, RAID and encrypted
systems. Support has also been added for the ext4 and btrfs
filesystems and &mdash; on the kFreeBSD architecture &mdash; the
Zettabyte filesystem (ZFS). The installation system for Debian
GNU/Linux is now available in 70 languages.</p>

<p>Debian installation images may be downloaded right now via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (the recommended method),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> or
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; see
<a href="$(HOME)/CD/">Debian on CDs</a> for further information.  It will
soon be available on physical DVD, CD-ROM and Blu-ray Discs from
numerous <a href="$(HOME)/CD/vendors">vendors</a>, too.</p>

<p>Upgrades to Debian GNU/Linux 6.0 from the previous release, Debian
GNU/Linux 5.0 (codenamed <q>Lenny</q>), are automatically handled by
the apt-get package management tool for most configurations, and to a
certain degree also by the aptitude package management tool.  As
always, Debian GNU/Linux systems may be upgraded painlessly, in place,
without any forced downtime, but it is strongly recommended to read
the <a href="$(HOME)/releases/squeeze/releasenotes">release notes</a> as
well as the <a href="$(HOME)/releases/squeeze/installmanual">installation guide</a>
for possible issues, and for detailed instructions on installing and
upgrading.  The release notes will be further improved and translated
to additional languages in the weeks after the release.</p>


<h2>ડેબિયન વિશે</h2>

<p>ડેબિયન એ ઈન્ટરનેટ દ્વારા સહયોગ કરીને સમગ્ર વિશ્વના ત્રણ હજાર કરતાં વધુ સ્વયંસેવકોનાં યોગદાન
દ્વારા બનાવવામાં આવેલ મુક્ત ઓપરેટિંગ સિસ્ટમ છે. ડેબિયન પ્રોજેક્ટની ચાવીરૂપ મજબૂતાઈઓ તેનો
સ્વયંસેવક પાયો, તેનું ડેબિયન સોશિઅલ કોન્ટ્રાક્ટ અને ફ્રી સોફ્ટવેર પ્રત્યેનું સમર્પણ, અને શક્ય ઉત્તમ
ઓપરેટિંગ સિસ્ટમ પૂરી પાડવાનો નિર્ધાર છે. ડેબિયન ૬.૦ એ આ દિશામાં અગત્યનું પગથિયું છે.</p>


<h2>સંપર્ક માહિતી</h2>

<p>વધુ માહિતી માટે, મહેરબાની કરી ડેબિયન વેબ પાનાંઓની 
<a href="$(HOME)/">http://www.debian.org/</a> પર મુલાકાત લો અથવા 
&lt;press@debian.org&gt; પર ઇમેલ કરો.</p>

