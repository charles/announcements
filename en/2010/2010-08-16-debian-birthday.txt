FOR IMMEDIATE RELEASE

The Debian Project is pleased to mark the 17th anniversary of its start.
As the official project history states[0]: "The Debian Project was 
officially founded by Ian Murdock on August 16th, 1993. At that time, 
the whole concept of a 'distribution' of Linux was new. Ian intended 
Debian to be a distribution which would be made openly, in the spirit 
of Linux and GNU".

Over the last seventeen years Debian has grown significantly. 
Initiated as a small project with just a handful of developers
it now has more than 1000 contributors from many corners 
of the globe. Documentation has been written in numerous languages,
and it now has tens of thousands of software packages, all following
the Debian Free Software Guidelines.[1]

Thanks to the contributions of individual developers, translators, 
graphic designers, system administrators, users and many others, 
Debian continues to thrive in the wider Free Software ecosystem. 
Debian is used by a number of "derivative" distributions that further 
expand the reach of Debian's software and Debian's philosophy of 
putting the user first. 

Under the auspices of Software In the Public Interest[2] and through 
the Debian Partners program[3] Debian continues to work with some of 
the largest names in hardware and software development. Debian has 
always striven to support a broad range of hardware with high-quality, 
well tested software and continues to work with numerous partners in 
various industries to do so. Debian is also widely used in the
computational and engineering disciplines in areas such as astronomy,
bioinformatics, and physics.

The Debian Project continues to welcome contributions in all forms, 
from users to developers, encouraging people to download, use, modify, 
and distribute its source code in the hopes that it is useful. The 
project will release Debian 6.0, code named "Squeeze", at some point 
this year once the hard work of fixing release critical bugs is 
finished. In the spirit of the Free Software community, Debian 
welcomes everyone to participate in this and all of Debian's 
activities and looks forward to the next seventeen years.

0. http://www.debian.org/doc/manuals/project-history/ch-intro.en.html
1. http://www.debian.org/social_contract#guidelines
2. http://www.spi-inc.org/about-spi
3. http://www.debian.org/partners/
