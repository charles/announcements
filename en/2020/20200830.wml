<define-tag pagetitle>DebConf20 online closes</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
On Saturday 29 August 2020, the annual Debian Developers
and Contributors Conference came to a close.
</p>

<p>
DebConf20 has been held online for the first time, due to the coronavirus 
(COVID-19) disease pandemic. 
</p>

<p>
All of the sessions have been streamed, with a variety of ways of participating:
via IRC messaging, online collaborative text documents,
and video conferencing meeting rooms.
</p>

<p>
With more than 850 attendees from 80 different countries and a
total of over 100 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings and other activities,
<a href="https://debconf20.debconf.org">DebConf20</a> was a large success.
</p>

<p>
When it became clear that DebConf20 was going to be an online-only
event, the DebConf video team spent much time over the next months to
adapt, improve, and in some cases write from scratch, technology that
would be required to make an online DebConf possible. After lessons
learned from the MiniDebConfOnline in late May, some adjustments were
made, and then eventually we came up with a setup involving Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad, and a newly written web-based
frontend for voctomix as the various elements of the stack.
</p>

<p>
All components of the video infrastructure are free software, and the
whole setup is configured through their public
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a> repository.
</p>

<p>
The DebConf20 <a href="https://debconf20.debconf.org/schedule/">schedule</a> included
two tracks in other languages than English: the Spanish language MiniConf,
with eight talks in two days,
and the Malayalam language MiniConf, with nine talks in three days.
Ad-hoc activities, introduced by attendees over the course of the entire conference,
have been possible too, streamed and recorded. There have also been several
team gatherings to <a href="https://wiki.debian.org/Sprints/">sprint</a> on certain Debian development areas.
</p>

<p>
Between talks, the video stream has been showing the usual sponsors on the loop, but also
some additional clips including photos from previous DebConfs, fun facts about Debian
and short shout-out videos sent by attendees to communicate with their Debian friends. 
</p>

<p>
For those who were not able to participate, most of the talks and sessions are already
available through the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">Debian meetings archive website</a>,
and the remaining ones will appear in the following days.
</p>

<p>
The <a href="https://debconf20.debconf.org/">DebConf20</a> website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> is planned to be held
in Haifa, Israel, in August or September.
</p>

<p>
DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Anti-Harassment team)
have been available to help so participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the <a href="https://debconf20.debconf.org/about/coc/">web page about the Code of Conduct in DebConf20 website</a>
for more details on this.
</p>

<p>
Debian thanks the commitment of numerous <a href="https://debconf20.debconf.org/sponsors/">sponsors</a>
to support DebConf20, particularly our Platinum Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
and 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>About Debian</h2>
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>About DebConf</h2>

<p>
DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>About Lenovo</h2>

<p>
As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, <a href="https://www.lenovo.com">Lenovo</a>
understands how critical open systems and platforms are to a connected world.
</p>

<h2>About Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 
</p>

<h2>About Google</h2>

<p>
<a href="https://google.com/">Google</a> is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.
</p>

<p>
Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of <a href="https://salsa.debian.org">Salsa</a>'s continuous integration infrastructure
within Google Cloud Platform.
</p>

<h2>About Amazon Web Services (AWS)</h2>

<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> is one of the world's
most comprehensive and broadly adopted cloud platforms,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.

<h2>Contact Information</h2>

<p>For further information, please visit the DebConf20 web page at
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
or send mail to &lt;press@debian.org&gt;.</p>
