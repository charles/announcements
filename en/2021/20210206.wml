<define-tag pagetitle>Updated Debian 10: 10.8 released</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the eighth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction atftp "Fix denial of service issue [CVE-2020-6097]">
<correction base-files "Update /etc/debian_version for the 10.8 point release">
<correction ca-certificates "Update Mozilla CA bundle to 2.40, blacklist expired <q>AddTrust External Root</q>">
<correction cacti "Fix SQL injection issue [CVE-2020-35701] and stored XSS issue">
<correction cairo "Fix mask usage in image-compositor [CVE-2020-35492]">
<correction choose-mirror "Update mirror list">
<correction cjson "Fix infinite loop in cJSON_Minify">
<correction clevis "Fix initramfs creation; clevis-dracut: Trigger initramfs creation upon installation">
<correction cyrus-imapd "Fix version comparison in cron script">
<correction debian-edu-config "Move host keytabs cleanup code out of gosa-modify-host into a standalone script, reducing LDAP calls to a single query">
<correction debian-installer "Use 4.19.0-14 Linux kernel ABI; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-installer-utils "Support partitions on USB UAS devices">
<correction device-tree-compiler "Fix segfault on <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Add missing build-dependency on tzdata">
<correction dovecot "Fix crash when searching mailboxes containing malformed MIME messages">
<correction dpdk "New upstream stable release">
<correction edk2 "CryptoPkg/BaseCryptLib: fix NULL dereference [CVE-2019-14584]">
<correction emacs "Don't crash with OpenPGP User IDs with no e-mail address">
<correction fcitx "Fix input method support in Flatpaks">
<correction file "Increase name recursion depth to 50 by default">
<correction geoclue-2.0 "Check the maximum allowed accuracy level even for system applications; make the Mozilla API key configurable and use a Debian-specific key by default; fix display of the usage indicator">
<correction gnutls28 "Fix test suite error caused by expired certificate">
<correction grub2 "When upgrading grub-pc noninteractively, bail out if grub-install fails; explicitly check whether the target device exists before running grub-install; grub-install: Add backup and restore; don't call grub-install on fresh install of grub-pc">
<correction highlight.js "Fix prototype pollution [CVE-2020-26237]">
<correction intel-microcode "Update various microcode">
<correction iproute2 "Fix bugs in JSON output; fix race condition that DOSes the system when using ip netns add at boot">
<correction irssi-plugin-xmpp "Do not trigger the irssi core connect timeout prematurely, thus fixing STARTTLS connections">
<correction libdatetime-timezone-perl "Update for new tzdata version">
<correction libdbd-csv-perl "Fix test failure with libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Security fix [CVE-2014-10402]">
<correction libmaxminddb "Fix heap-based buffer over-read [CVE-2020-28241]">
<correction lttng-modules "Fix build on kernel versions &gt;= 4.19.0-10">
<correction m2crypto "Fix compatibility with OpenSSL 1.1.1i and newer">
<correction mini-buildd "builder.py: sbuild call: set '--no-arch-all' explicitly">
<correction net-snmp "snmpd: Add cacheTime and execType flags to EXTEND-MIB">
<correction node-ini "Do not allow invalid hazardous string as section name [CVE-2020-7788]">
<correction node-y18n "Fix prototype pollution issue [CVE-2020-7774]">
<correction nvidia-graphics-drivers "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction pdns "Security fixes [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Turn into a dummy package taking care of removing the previously installed plugin (no longer functional nor supported)">
<correction pngcheck "Fix buffer overflow [CVE-2020-27818]">
<correction postgresql-11 "New upstream stable release; security fixes [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Ensure timestamp tags aren't too long before trying to decode them [CVE-2020-35573]">
<correction python-bottle "Stop allowing <q>;</q> as a query-string separator [CVE-2020-28473]">
<correction python-certbot "Automatically use ACMEv2 API for renewals, to avoid issues with ACMEv1 API removal">
<correction qxmpp "Fix potential SEGFAULT on connection error">
<correction silx "python(3)-silx: Add dependency on python(3)-scipy">
<correction slirp "Fix buffer overflows [CVE-2020-7039 CVE-2020-8608]">
<correction steam "New upstream release">
<correction systemd "journal: do not trigger assertion when journal_file_close() is passed NULL">
<correction tang "Avoid race condition between keygen and update">
<correction tzdata "New upstream release; update included timezone data">
<correction unzip "Apply further fixes for CVE-2019-13232">
<correction wireshark "Fix various crashes, infinite loops and memory leaks [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2021 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction compactheader "Incompatible with current Thunderbird versions">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
