<define-tag pagetitle>Updated Debian 11: 11.9 released</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the ninth update of its
oldstable distribution Debian <release> (codename <q><codename></q>).
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This oldstable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction axis "Filter out unsupported protocols in the client class ServiceFactory [CVE-2023-40743]">
<correction base-files "Update for the 11.9 point release">
<correction cifs-utils "Fix non-parallel builds">
<correction compton "Remove recommendation of picom">
<correction conda-package-handling "Skip unreliable tests">
<correction conmon "Do not hang when forwarding container stdout/stderr with lots of output">
<correction crun "Fix containers with systemd as their init system, when using newer kernel versions">
<correction debian-installer "Increase Linux kernel ABI to 5.10.0-28; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-ports-archive-keyring "Add Debian Ports Archive Automatic Signing Key (2025)">
<correction debian-security-support "Mark tor, consul and xen as end-of-life; limit samba support to non-AD DC use cases; match golang packages with regular expression; drop version-based checking; add chromium to security-support-ended.deb11; add tiles and libspring-java to security-support-limited">
<correction debootstrap "Backport merged-/usr support changes from trixie: implement merged-/usr by post-merging, default to merged-/usr for suites newer than bookworm in all profiles">
<correction distro-info "Update tests for distro-info-data 0.58+deb12u1, which adjusted Debian 7's EoL date">
<correction distro-info-data "Add Ubuntu 24.04 LTS Noble Numbat; fix several End Of Life dates">
<correction dpdk "New upstream stable release">
<correction dropbear "Fix security measure bypass issue [CVE-2021-36369]; fix <q>terrapin</q> attack [CVE-2023-48795]">
<correction exuberant-ctags "Fix arbitrary command execution issue [CVE-2022-4515]">
<correction filezilla "Prevent <q>terrapin</q> exploit [CVE-2023-48795]">
<correction gimp "Remove old versions of separately packaged dds plugin">
<correction glib2.0 "Align with upstream stable fixes; fix denial of service issues [CVE-2023-32665 CVE-2023-32611 CVE-2023-29499 CVE-2023-32636]">
<correction glibc "Fix a memory corruption in <q>qsort()</q> when using nontransitive comparison functions.">
<correction gnutls28 "Security fix for timing sidechannel attack [CVE-2023-5981]">
<correction imagemagick "Various security fixes [CVE-2021-20241 CVE-2021-20243 CVE-2021-20244 CVE-2021-20245 CVE-2021-20246 CVE-2021-20309 CVE-2021-3574 CVE-2021-39212 CVE-2021-4219 CVE-2022-1114 CVE-2022-28463 CVE-2022-32545 CVE-2022-32546]">
<correction jqueryui "Fix cross-site scripting issue [CVE-2022-31160]">
<correction knewstuff "Ensure correct ProvidersUrl to fix denial of service">
<correction libdatetime-timezone-perl "Update included timezone data">
<correction libde265 "Fix segmentation violation in the function <q>decoder_context::process_slice_segment_header</q> [CVE-2023-27102]; fix heap buffer overflow in the function <q>derive_collocated_motion_vectors</q> [CVE-2023-27103]; fix buffer over-read in <q>pic_parameter_set::dump</q> [CVE-2023-43887]; fix buffer overflow in the <q>slice_segment_header</q> function [CVE-2023-47471]; fix buffer overflow issues [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libmateweather "Update included location data; update data server URL">
<correction libpod "Fix incorrect handling of supplementary groups [CVE-2022-2989]">
<correction libsolv "Enable zstd compression support">
<correction libspreadsheet-parsexlsx-perl "Fix possible memory bomb [CVE-2024-22368]; fix XML External Entity issue [CVE-2024-23525]">
<correction linux "New upstream stable release; increase ABI to 28">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 28">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 28">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 28">
<correction llvm-toolchain-16 "New backported package to support builds of newer chromium versions; build-dep on <q>llvm-spirv</q> instead of <q>llvm-spirv-16</q>">
<correction mariadb-10.5 "New upstream stable release; fix denial of service issue [CVE-2023-22084]">
<correction minizip "Reject overflows of zip header fields [CVE-2023-45853]">
<correction modsecurity-apache "Fix protection bypass issues [CVE-2022-48279 CVE-2023-24021]">
<correction nftables "Fix incorrect bytecode generation">
<correction node-dottie "Fix prototype pollution issue [CVE-2023-26132]">
<correction node-url-parse "Fix authorisation bypass issue [CVE-2022-0512]">
<correction node-xml2js "Fix prototype pollution issue [CVE-2023-0842]">
<correction nvidia-graphics-drivers "New upstream release [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release [CVE-2023-31022]">
<correction opendkim "Properly delete Authentication-Results headers [CVE-2022-48521]">
<correction perl "Prevent buffer overflow via illegal Unicode property [CVE-2023-47038]">
<correction plasma-desktop "Fix denial of service bug in discover">
<correction plasma-discover "Fix denial of service bug; fix build failure">
<correction postfix "New upstream stable release; address SMTP smuggling issue [CVE-2023-51764]">
<correction postgresql-13 "New upstream stable release; fix SQL injection issue [CVE-2023-39417]">
<correction postgresql-common "Fix autopkgtests">
<correction python-cogent "Skip parallel tests on single-CPU systems">
<correction python-django-imagekit "Avoid triggering path traversal detection in tests">
<correction python-websockets "Fix predictable duration issue [CVE-2021-33880]">
<correction pyzoltan "Build on single core systems">
<correction ruby-aws-sdk-core "Include VERSION file in package">
<correction spip "Fix cross-site scripting issue">
<correction swupdate "Prevent acquiring root privileges through inappropriate socket mode">
<correction symfony "Ensure CodeExtension's filters properly escape their input [CVE-2023-46734]">
<correction tar "Fix boundary checking in base-256 decoder [CVE-2022-48303], handling of extended header prefixes [CVE-2023-39804]">
<correction tinyxml "Fix assertion issue [CVE-2023-34194]">
<correction tzdata "Update included timezone data">
<correction unadf "Fix stack buffer overflow issue [CVE-2016-1243]; fix arbitary code execution issue [CVE-2016-1244]">
<correction usb.ids "Update included data list">
<correction vlfeat "Fix FTBFS with newer ImageMagick">
<correction weborf "Fix denial of service issue">
<correction wolfssl "Fix buffer overflow issues [CVE-2022-39173 CVE-2022-42905], key disclosure issue [CVE-2022-42961], predictable buffer in input keying material [CVE-2023-3724]">
<correction xerces-c "Fix use-after-free issue [CVE-2018-1311]; fix integer overflow issue [CVE-2023-37536]">
<correction zeromq3 "Fix <q>fork()</q> detection with gcc 7; update copyright relicense statement">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the oldstable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5509 firefox-esr>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5522 tomcat9>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5530 ruby-rack>
<dsa 2023 5531 roundcube>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5537 openjdk-11>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5554 postgresql-13>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5594 linux-signed-amd64>
<dsa 2024 5594 linux-signed-arm64>
<dsa 2024 5594 linux-signed-i386>
<dsa 2024 5594 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5604 openjdk-11>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>


<h2>Removed packages</h2>

<p>The following obsolete package was removed from the distribution:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction gimp-dds "Integrated in gimp >=2.10">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into oldstable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current oldstable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Proposed updates to the oldstable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>oldstable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


