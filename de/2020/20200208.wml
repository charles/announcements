<define-tag pagetitle>Debian 10 aktualisiert: 10.3 veröffentlicht</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="f5d37669b70c6079f38797f657ae24a4f9e5885d" maintainer="Erik Pfannenstein"


<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die dritte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction alot "Ablaufdatum der Testsuiteschlüssel entfernt, um Kompilierungsfehlschläge zu beheben">
<correction atril "Speicherzugriffsfehler behoben, wenn kein Dokument geladen ist; Einlesen von nicht initialisiertem Speicher behoben [CVE-2019-11459]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction beagle "Statt Symlinks auf JAR-Dateien ein Wrapper-Skript anbieten, sodass die JARs wieder funktionieren">
<correction bgpdump "Speicherzugriffsfehler behoben">
<correction boost1.67 "Undefiniertes Verhalten behoben, welches zum Absturz von libboost-numpy geführt hat">
<correction brightd "Den von /sys/class/power_supply/AC/online ausgelesenen Wert wirklich mit <q>0</q> vergleichen">
<correction casacore-data-jplde "Tabellen bis 2040 inkludiert">
<correction clamav "Neue Version der Originalautoren; Dienstblockadeproblem behoben [CVE-2019-15961]; ScanOnAccess-Option entfernt und mit clamonacc ersetzt">
<correction compactheader "Neue Version der Originalautoren, kompatibel mit Thunderbird 68">
<correction console-common "Rückschritt behoben, der dazu geführt hat, dass Dateien nicht eingebunden wurden">
<correction csh "Speicherzugriffsfehler bei eval behoben">
<correction cups "Speicherleck in ppdOpen behoben; Validierung der Standardsprache in ippSetValuetag überarbeitet [CVE-2019-2228]">
<correction cyrus-imapd "Der cyrus-upgrade-db einen BACKUP-Type hinzufügen, um Upgrade-Probleme loszuwerden">
<correction debian-edu-config "Bisherige Proxy-Einstellungen auf dem Client beibehalten, falls WPAD nicht erreichbar ist">
<correction debian-installer "Neukompilierung gegen proposed-updates; Generierung der mini.iso auf arm abgeändert, damit EFI-Netzwerk-Boot funktioniert; USE_UDEBS_FROM-Vorgabe von Unstable auf Buster abgeändert, um Benutzern zu helfen, die lokal kompilieren">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-security-support "Status der Sicherheitsunterstützung einiger Pakete aktualisiert">
<correction debos "Neukompilierung gegen aktualisierte golang-github-go-debos-fakemachine">
<correction dispmua "Neue Version der Originalautoren, kompatibel mit Thunderbird 68">
<correction dkimpy "Neue stabile Veröffentlichung der Originalautoren">
<correction dkimpy-milter "Fix privilege management at startup so Unix sockets work">
<correction dpdk "Neue stabile Veröffentlichung der Originalautoren">
<correction e2fsprogs "Potenziellen Stapelunterlauf in e2fsck behoben [CVE-2019-5188]; Use-after-free in e2fsck behoben">
<correction fig2dev "Fig-v2-Textzeichenketten erlauben, die mit mehreren ^A enden [CVE-2019-19555]; riesige Arrow Types blockieren, die zu Ganzzahl-Überläufen führen [CVE-2019-19746]; meherere Abstürze behoben [CVE-2019-19797]">
<correction freerdp2 "realloc-Rückgabe-Handhabung überarbeitet [CVE-2019-17177]">
<correction freetds "tds: Sichergehen, dass UDT varint auf 8 gesetzt hat [CVE-2019-13508]">
<correction git-lfs "Kompilierungsprobleme mit neueren Go-Versionen behoben">
<correction gnubg "Größe der statischen Puffer, die dazu verwendet werden, Nachrichten während des Programmstarts zu generieren, gesteigert, sodass wegen der Spanisch-Übersetzung kein Puffer überläuft">
<correction gnutls28 "Interop-Probleme mit gnutls 2.x behoben; Auswertung von Zertifikaten unter Verwendung der RegisteredID überarbeitet">
<correction gtk2-engines-murrine "Ko-Installierbarkeit neben anderen Themes verbessert">
<correction guile-2.2 "Kompilierungsfehlschlag behoben">
<correction libburn "<q>cdrskin multi-track burning was slow and stalled after track 1</q> behoben">
<correction libcgns "Kompilierungsfehlschlag auf ppc64el behoben">
<correction libimobiledevice "Richtig mit teilweisem SSL-Schreiben umgehen">
<correction libmatroska "Shared-Library-Abhängigkeit auf 1.4.7 geändert, weil diese Version neue Symbole eingeführt hat">
<correction libmysofa "Sicherheitskorrekturen [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libparse-win32registry-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libperl4-corelibs-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libsolv "Heap-Pufferüberlauf behoben [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Bisher unbenutzbare OpenDocument-Tabellen korrigiert, ebenso die Weitergabe von JSON-Formatierungsoptionen">
<correction libtimedate-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libvirt "Apparmor: das Ausführen von pygrub erlauben; osxsave und ospke nicht in der QEMU-Befehlszeile erscheinen lassen; dies hilft neueren QEMU-versionen mit einigen Konfigurationen, die von virt-install erzeugt werden">
<correction libvncserver "RFBserver: keinen Stack-Speicher an die Gegenstelle durchsickern lassen [CVE-2019-15681]; Einfrieren beim Schließen von Verbindungen und einen Speicherzugriff auf Multi-Thread-VNC-Servern behoben; Problem beim Verbinden mit VMWare-Servern behoben; Absturz von x11vnc, wenn vncviewer eine Verbindung aufbaut, behoben">
<correction limnoria "Fern-Informationsoffenlegung und mögliche Fern-Codeausführung im Math-Plugin behoben [CVE-2019-19010]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Aktualisierung für das 4.19.0-8 Linux-Kernel-ABI">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction mariadb-10.3 "Neue stabile Veröffentlichung der Originalautoren [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "shmget() mit Berechtigung 0600 statt 0777 aufrufen [CVE-2019-5068]">
<correction mnemosyne "Fehlende Abhängigkeit von PIL hinzugefügt">
<correction modsecurity "Cookie-Kopfzeilen-Auswertefehler behoben [CVE-2019-19886]">
<correction node-handlebars "Direktaufruf von <q>helperMissing</q> und <q>blockHelperMissing</q> verbieten [CVE-2019-19919]">
<correction node-kind-of "Type-Checking-Schwachstelle in ctorName() behoben [CVE-2019-20149]">
<correction ntpsec "Langsame DNS-Neuversuche behoben; ntpdate -s (syslog) überarbeitet, um den if-up-Hook zu korrigieren; Korrekturen in der Dokumentation">
<correction numix-gtk-theme "Ko-Installierbarkeit neben anderen Themes korrigiert">
<correction nvidia-graphics-drivers-legacy-340xx "Neue stabile Veröffentlichung der Originalautoren">
<correction nyancat "Neukompilierung in sauberer Umgebung, um die systemd-Unit für nyancat-server hinzuzufügen">
<correction openjpeg2 "Heap-Überlauf [CVE-2018-21010] und Ganzzahlüberlauf behoben [CVE-2018-20847]">
<correction opensmtpd "Benutzer vor den Änderungen der smtpd.conf-Syntax warnen (in früheren Versionen); smtpctl setgid opensmtpq installieren; während der Konfigurierungsphase mit hostname-Rückgabestatus ungleich null umgehen können">
<correction openssh "Ipc in der seccomp-Sandbox (nicht-fatal) ablehnen, um Störungen mit OpenSSL 1.1.1d und Linux &lt; 3.19 auf einigen Architekturen zu beheben">
<correction php-horde "Stored-Cross-Site-Scripting-Problem in Horde Cloud Block behoben [CVE-2019-12095]">
<correction php-horde-text-filter "Ungültige reguläre Ausdrücke korrigiert">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren">
<correction postgresql-11 "Neue stabile Veröffentlichung der Originalautoren">
<correction print-manager "Absturz, wenn CUPS dieselbe ID für mehrere Druckaufträge zurückliefert, behoben">
<correction proftpd-dfsg "CRL-Probleme behoben [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Pfade zu den Fonts korrigiert">
<correction python-evtx "Import von <q>hexdump</q> überarbeitet">
<correction python-internetarchive "Nach dem Auslesen des Hashes die Datei schließen, damit die Dateideskriptoren nicht knapp werden">
<correction python3.7 "Sicherheitskorrekturen [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Unterstützung für Nicht-PPD-drucker hinzufügen und stillschweigenden Rückgriff auf einen PPD-fähigen Drucker vermeiden; Absturz bei Verwendung von QLabels mit Rich-Text; Grafik-Tablett-Hover-Events überarbeitet">
<correction qtwebengine-opensource-src "PDF-Auswertung überarbeitet; ausführbaren Stack abgeschaltet">
<correction quassel "quasselcore-AppArmor-Blockaden beim Speichern der Konfiguration behoben; Standardkanal für Debian korrigiert; unnötige NEWS-Datei entfernt">
<correction qwinff "Absturz durch inkorrekte Dateierkennung behoben">
<correction raspi3-firmware "Erkennung der seriellen Konsole mit Kernel 5.x korrigiert">
<correction ros-ros-comm "Sicherheitsprobleme behoben [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "Neue stabile Veröffentlichung der Originalautoren; unsichere Berechtigungen im enigma-Plugin korrigiert [CVE-2018-1000071]">
<correction schleuder "Erkennung von Schlüsselwörtern in Mails mit <q>geschützten Kopfzeilen</q> und leerem Betreff überarbeitet; Nicht-Eigensignaturen beim Aktualisieren oder Abrufen von Schlüsseln entfernen; Fehler melden, wenn das Argument, das an `refresh_keys` übergeben wird, keine existierende Liste ist; fehlende List-Id-Kopfzeile zu den Benachrichtigungs-Mails für die Admins hinzufügen; Entschlüsselungsprobleme souverän behandeln; standardmäßig ASCII-8BIT-Kodierung verwenden">
<correction simplesamlphp "Inkompatibiltät mit PHP 7.3 behoben">
<correction sogo-connector "Neue Version der Originalautoren, kompatibel mit Thunderbird 68">
<correction spf-engine "Privilegienverwaltung beim Starten überarbeitet, um die Unix-Sockets zum Laufen zu kriegen; Dokumentation für TestOnly aktualisiert">
<correction sudo "Einen (in Buster nicht ausnutzbaren) Pufferüberlauf, wenn pwfeedback eingeschaltet und das Eingabegerät keine TTY ist, behoben [CVE-2019-18634]">
<correction systemd "fs.file-max sysctl auf LONG_MAX statt auf ULONG_MAX setzen; Besitzer/Modi der Ausführungsverzeichnisse ebenso für statische Benutzer ändern, sodass die Ausführungsverzeichnisse wie CacheDirectory und StateDirectory für den Benutzer, der in User= angegeben ist, ordnungsgemäß gechowned werden, bevor der Dienst gestartet wird">
<correction tifffile "Wrapper-Skript korrigiert">
<correction tigervnc "Sicherheitskorrekturen [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Sicherheitskorrekturen [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Pfade zu ip(6)tables-restore wegen der Migration zu nftables korrigiert">
<correction unhide "Stack-Erschöpfung behoben">
<correction x2goclient "~/, ~user{,/}, ${HOME}{,/} und $HOME{,/} im SCP-Modus vom Zielpfad entfernen; behebt Regression mit neueren libssh-Versionen, bei denen Korrekturen für CVE-2019-14889 angewendet worden sind">
<correction xmltooling "Race-Condition behoben, die unter Last zum Absturz führen konnte">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction caml-crush "[armel] Unkompilierbar wegen des Fehlens von ocaml-native-compilers">
<correction firetray "Inkompatibel mit derzeitigen Thunderbird-Versionen">
<correction koji "Sicherheitsprobleme">
<correction python-lamson "Defekt wegen Änderungen in python-daemon">
<correction radare2 "Sicherheitsprobleme; Originalautoren bieten keine Stable-Unterstützung">
<correction radare2-cutter "Hängt von radare2 ab, das entfernt wird">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>


<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühnungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>

