# Status: open-for-edit
# $Id$
# $Rev$

<define-tag pagetitle>Debian 6.0 không còn được hỗ trợ dài hạn nữa</define-tag>
<define-tag release_date>2016-02-12</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1" maintainer="Trần Ngọc Quân <vnwildman@gmail.com>"

<p>Nhóm hỗ trợ dài hạn Debian (LTS) Team thông báo rằng việc hỗ trợ Debian 6.0
(<q>squeeze</q>) sẽ hết vòng đời vào 29 tháng hai, 2016,
năm năm sau khi nó được phát hành lần đầu vào ngày 6 tháng hai, 2011.</p>

<p>Sẽ không hỗ trợ bảo mật thêm nữa với Debian 6.0.</p>

<p>Nhóm LTS sẽ chuẩn bị chuyển sang Debian 7 (<q>wheezy</q>), cái mà
hiện tại là bản phát hành ổn định cũ. Nhóm LTS sẽ nhận hỗ trợ từ nhóm Bảo mật
vào 26 tháng tư, 2016.</p>

<p>Debian 7 sẽ đồng thời nhận được hỗ trợ dài hạn trong năm năm sau khi nó được
phát hành lần đầu và kết thúc vào tháng năm 2018.</p>

<p>Debian và nhóm LTS của nó muốn cảm ơn tới những người đóng góp,
những nhà phát triển và các nhà bảo trợ những người mà làm cho nó có thể kéo dài
vòng đời của các bản phát hành ổn định trước đây, và những người giúp cho LTS này thành công.</p>

<p>Nếu bạn tin tưởng vào Debian LTS, vui lòng cân nhắc
<a href="https://wiki.debian.org/LTS/Development">gia nhập nhóm</a>,
cung cấp miếng vá, thử nghiệm hay
<a href="https://wiki.debian.org/LTS/Funding">lập các quỹ</a>.</p>

<h2>Vài nét về Debian</h2>

<p>
Dự án Debian được thành lập vào năm 1993 bởi Ian Murdock để trở thành một  
cộng đồng phần mềm tự do đích thực. Từ đó dự án đã phát
triển thành một trong những dự án mã nguồn mở có ảnh
hưởng nhất. Hàng ngàn tình nguyện viên từ khắp nơi
trên thế giới cùng nhau góp sức để tạo nên và duy trì
phần mềm Debian. Với 70 ngôn ngữ sẵn có, và có thể hỗ
trợ đa dạng các loại máy tính, Debian tự gọi bản thân
là một <q>hệ điều hành toàn cầu</q>.
</p>
<p>
Dự án Debian là một hiệp hội các nhà phát triển phần mềm tự do,
gồm những người tình nguyện dành thời gian cá nhân cùng nỗ lực
của bản thân để xây dựng một hệ điều hành hoàn toàn tự do Debian GNU/Linux.
</p>

<h2>Thông tin thêm</h2>

<p>Các thông tin chi tiết hơn nữa về hỗ trợ dài hạn Debian có thể tìm thấy tại
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>

<h2>Thông tin liên hệ</h2>

<p>
Để biết thêm thông tin, xin vui lòng truy cập trang web của Debian
tại <a href="$(HOME)/">https://www.debian.org/</a> hoặc gửi email đến địa chỉ
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
